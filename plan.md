## Plano de Aula

### 25/03

#### 08:30
* Introdução 0:15
* Tipos de dados 0:15
* Tipos de variáveis 0:15
* Number, String, Boolean, Symbol, undefined, null 0:30
* RegExp 0:30
* Exercício 1 0:15

#### 10:30
* Coffee Break 0:15

#### 10:45
* Correção do Exercício 1 0:15
* Object 1:15

#### 12:15
* Almoço 01:30

#### 13:45
* Exercício 2 0:15
* Correção do Exercício 2 0:15
* Function - Parte 1 1:00
* Arrow Function 0:15
* Destructuring 0:30

#### 16:00
* Coffee Break 0:15

#### 16:15
* Exercício 3 0:15
* Correção do Exercício 3 0:15
* Function - Parte 2 1:00
* Execution Context e Closure 0:30

### 26/03

#### 08:30
* Errors 0:15
* Exercício 4 0:30
* Correção do Exercício 4 0:15
* Classes 0:45
* Proxy e Reflect 0:30

#### 10:15
* Coffee Break 0:15

#### 10:30
* Exercício 5 0:30
* Correção do Exercício 5 0:15
* Modules 0:30


#### 11:45
* Almoço 01:30

#### 13:15
* Exercício 6 0:45
* Correção do Exercício 6 0:15
* Array Map Set WeakMap WeakSet 1:00

#### 15:30
* Coffee Break 0:15

#### 15:45
* Exercício 7 0:45
* Correção do Exercício 7 0:15
* Promises 0:15
* Generators 0:15
* Exercício 8 0:30
* Correção do Exercício 8 0:15